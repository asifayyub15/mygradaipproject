# mygradproject
   
 Step to run my Application

    1. Create Database
        create database travelworld;
        use travelworld;
    2. Create Table which holds Cities List
         from .sql folder
    3. Insert cities list
         from .sql folder
    4. Open Application in Eclipse
    5. MVN Install
    6. Run As Spring boot Application
    7. Open Browser and visit
     localhost:8080

 Features ->

 The table shows the following columns
 First Name, Last Name, Trip Type, Origin, Destination
 Trip Type will be either "One Way" or "Round Trip"

 If there are no trips in the database, the table header will still show, but the next row spans the length of the table and reads "No Records Found"

 Above the table is a button that reads: "New Trip"

When the user clicks the button, they are taken to a new page with a form to build their trip

 The user must select an origin and a destination for their trip, decide if it is a one-way trip or round trip, and enter their name.

 The user can select a city and state from the first dropdown menu (this is their origin). 
 The drop down should contain at least 10 US Cities and their corresponding state. 
 The first option in this dropdown is "Select an origin"
 This is an invalid option

 The user can select a city and state from the second dropdown menu (this is their destination). 
 The drop down should contain at least 10 US Cities and their corresponding state. 
 The first option in this dropdown is "Select a destination"
 This is an invalid option

 The user must decide if it is a one-way trip or a round trip
 Provide 2 radio buttons. You can only select from one of two options
 One Way
 Round Trip

 The user will also enter their first and last name

 An error is presented if any of the following is true
 The user did not select an origin or a destination
 The user only selects an origin
 The user only selects a destination
  The user selects the same origin and destination
  "One Way" is not selected, and "Round Trip" is not selected
 First name is not filled in
 Last name is not filled in

 Once the form is submitted, the user is taken back to the main page where a table shows a list of all bookings.

hightlight feature:
Validation on Name, Email, Mobile Number
Trip Date should be a future date only
When user select Destination city that city is removed from that Origin auto maticaly
We can search the trip data from list also







